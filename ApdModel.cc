#include "ApdModel.h"

#include <TRandom.h>

// returns number of cells fired for given number of shotted photons
Int_t Apd::SampleEvent(Int_t nPhot)
{
  //reset pixels map and counter
  fMap = 0;
  fCnt = 0;
  
  // loop over number of photons
  for (Int_t i = 0; i < nPhot; i++) {
    // sample pixel
    const Int_t x = Int_t(fNx*gRandom->Rndm());
    const Int_t y = Int_t(fNy*gRandom->Rndm());
    SampleOnePhoton(x, y);
  }
  
  return fCnt;
}

void Apd::SampleOnePhoton(Int_t x, Int_t y)
{
  // check x,y is within grid
  if (x < 0) return;
  if (x >= fNx) return;
  if (y < 0) return;
  if (y >= fNy) return;
  
  // the pixel is already fired => photon produces nothing
  if (fMap(x, y) == 1) return;
  
  // mark the pixel as fired
  fMap(x, y) = 1;
  fCnt++;
  
  // simulate cross current from this pixel
  CrossCurrent(x, y);
}

void Apd::CrossCurrent(Int_t x, Int_t y)
{
  if (fProbC == 0.) return;
  
  // cross pixels
  if (gRandom->Rndm() < fProbC) SampleOnePhoton(x, y+1);
  if (gRandom->Rndm() < fProbC) SampleOnePhoton(x, y-1);
  if (gRandom->Rndm() < fProbC) SampleOnePhoton(x+1, y);
  if (gRandom->Rndm() < fProbC) SampleOnePhoton(x-1, y);
  
  // diagonal pixels
  if (gRandom->Rndm() < fProbD) SampleOnePhoton(x+1, y+1);
  if (gRandom->Rndm() < fProbD) SampleOnePhoton(x-1, y+1);
  if (gRandom->Rndm() < fProbD) SampleOnePhoton(x+1, y-1);
  if (gRandom->Rndm() < fProbD) SampleOnePhoton(x-1, y-1);
}
