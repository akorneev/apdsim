@echo off
set zip="c:\Program Files\7-Zip\7z.exe"

rem extract current svn revision
for /F %%i in ('git log -1 --date^=format:%%Y%%m%%d --format^=format:%%ad.%%h') do set revision=%%i
set dist=apdsim-%revision%
echo Creating distributive %dist% ...

rem cook arhive
mkdir %dist% || exit /B %ERRORLEVEL%
copy apdsim.exe %dist%\
copy .rootrc %dist%\
xcopy macro %dist%\macro /E /I
mkdir %dist%\rate
copy rate\rate.cxx %dist%\rate
%zip% a -tzip %dist%.zip %dist%\
rmdir /s /q %dist%

echo Distributive is ready: %dist%.zip
