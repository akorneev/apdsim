#include <TGFrame.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TGStatusBar.h>
#include <TGProgressBar.h>
#include <TGNumberEntry.h>
#include <TGComboBox.h>

class MainFrame: public TGMainFrame 
{
public:
  MainFrame(const TGWindow* p, UInt_t w, UInt_t h);
  
  //slots
  void DoSingle();
  void DoAll();
  void DoSave();
  
  virtual void CloseWindow()
  {
    Cleanup();
    gApplication->Terminate(0);
  }
  
protected:
  
  void redraw(TGWindow* window);
  void setStatus(const char* status);
  
  TGNumberEntry* fNmean;
  TGComboBox* comboDistribution;
  
  TGNumberEntry* fNx;
  TGNumberEntry* fNy;
  TGNumberEntry* entryCrossProb;
  TGNumberEntry* entryCrossRatio;
  
  TGNumberEntry* fNevt;
  TGTextButton* buttonSingle;
  
  TGNumberEntry* entryFrom;
  TGNumberEntry* entryTo;
  TGNumberEntry* entryStep;
  TGTextButton* buttonAll;
  
  TCanvas* fCan1;
  TCanvas* fCan2;
  
  TGStatusBar* statusBar;
  
  ClassDef(MainFrame,0)
};
