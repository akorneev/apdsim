Usage on LXPLUS:
-----------------

$ ssh lxplus.cern.ch
$ git clone https://gitlab.cern.ch/akorneev/apdsim.git
$ cd apdsim
$ root
[] .L ApdModel.cc+
[] .L MainFrame.cc+
[] apd()


Build instruction:
------------------

* Install ROOT 5.34.38
  https://root.cern.ch/download/root_v5.34.38.win32.vc12.exe
  Select the option "Add ROOT to the system PATH for all users" during installation.

* Install Visual Studio 2015
  available through CERM CMF system

* Install Git client
  https://git-scm.com/downloads

* Open VS command prompt
  run Start ->
    Programs ->
      Visual Studio 2015 ->
        Visual Studio Tools ->
          Developer Command Prompt

* Checkout / compile sources / run program
  > git clone https://gitlab.cern.ch/akorneev/apdsim.git
  > cd apdsim
  > nmake -f Makefile.win
  > apdsim.exe
  > cd macro
  > nmake -f Makefile.win
  > sipmnoise.exe

* Build distributive:
  > nmake dist


Code repository:
----------------

  https://gitlab.cern.ch/akorneev/apdsim

Windows releases:
-----------------

* Latest distribution available at:
  http://cern.ch/begemot/apdsim/apdsim-r339.zip
