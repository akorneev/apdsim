#include "MainFrame.h"
#include <TApplication.h>

#include <windows.h>

int main(int argc, char* argv[])
{
  TApplication* app = new TApplication("APDSIM", &argc, argv);
  new MainFrame(0, 800, 700);
  
  app->Run(kTRUE);
  return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
  return main(__argc, __argv);
}
