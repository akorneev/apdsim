#include "TCanvas.h"
#include "TF1.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TList.h"

#include <iostream>
#include <fstream>

// Reference:
//   Electric field effect on the thermal emission of traps in semiconductor junctions
//   http://dx.doi.org/10.1063/1.326601

// Usage:
//   root
//   .L rate.cxx
//   
// temperature scan 200 - 350 K:
//   rate_vs_temperature(200, 350, 2E7, 0.56);
  
// electric field scan 1E6 - 1E9
//   rate_vs_efield(200, 1E6, 1E9, 0.56);
  
// single point
//   rate_func(100, 1E7, 0.56).print();


Double_t func1(Double_t* x, Double_t* par)
{
  const Double_t z = x[0];
  const Double_t C1 = par[0];
  const Double_t dEi_kT = par[1];
  
  /*
  cout << "func1():"
       << " z = " << z
       << " C1 = " << C1
       << " dEi_kT = " << dEi_kT
       << " aaa = " << z - pow(z, 1.5) * C1 * (1 - pow(dEi_kT / z, 5./3.))
       << endl;
  */
  
  return exp(z - pow(z, 1.5) * C1 * (1 - pow(dEi_kT / z, 5./3.)));
}

struct RateParameters {
  double T;
  double Efield;
  double Eion;
  double m;
};

struct RateResult {
  RateParameters params;
  
  double poole_frenkel;
  double tunneling;
  double total() const {return (poole_frenkel + tunneling);}
  void print() const { cout << "poole_frenkel = " << poole_frenkel << " tunneling = " << tunneling << " total = " << total() << endl; }
};


// input parameters:
//   T      - temperature, K
//   Efield - electric field, V/m
//   Eion   - ionisation energy, eV
RateResult rate_func(const double T, const double Efield, const double Eion)
{
  const double q = 4.8E-10;
  const double F = 1E-4/3. * Efield;
  const double pi = 3.141592;
  const double eps = 11.9;
  const double k = 1.38E-16;
  const double m = 1.07E-27;
  const double h = 1.05E-27;
  const double Ei = 1.602E-12 * Eion;
  
  const double dEi = q * sqrt(q * F / pi / eps);
  const double kT = k * T;
  const double C1 = (4./3. * sqrt(m) * pow(kT, 1.5) / (q * h * F));
  
  TF1* fint = new TF1("func1", func1, 0, 1, 2);
  fint->SetParameters(C1, dEi/kT);
  fint->SetParNames("C1", "dEi/kT");
  
  /*
  cout << "dEi/kT = " << dEi/kT << "\n"
       << "Ei/kT = " << Ei/kT << "\n"
       << "fint(10) = " << fint->Eval(10)
       << endl;
  */
  
  const double poole_frenkel = exp(dEi/kT);
  const double tunneling = fint->Integral(dEi/kT, Ei/kT);
  
  const RateParameters p = {T, Efield, Eion, m};
  const RateResult r = {p, poole_frenkel, tunneling};
  
  /*
  cout << "poole_frenkel = " << poole_frenkel
       << " tunneling = " << tunneling
       << " summ = " << (poole_frenkel + tunneling) << " ";
  */
  
  return r;
}

void rate_vs_temperature(const double T_min, const double T_max, const double Efield, const double Eion)
{
  TGraph* gTotal = new TGraph;
  gTotal->SetTitle("total");
  gTotal->SetFillColor(0);
  gTotal->SetLineWidth(2);
  
  TGraph* gPF = new TGraph;
  gPF->SetTitle("poole_frenkel");
  gPF->SetFillColor(0);
  gPF->SetLineColor(kBlue);
  gPF->SetLineWidth(2);
  
  TGraph* gT = new TGraph;
  gT->SetTitle("tunneling");
  gT->SetFillColor(0);
  gT->SetLineColor(kRed);
  gT->SetLineWidth(2);
  
  TMultiGraph* mg = new TMultiGraph;
  mg->SetTitle("Emission rate;Temperature, K;Relative rate");
  mg->Add(gTotal);
  mg->Add(gPF);
  mg->Add(gT);
  
  int n = 0;
  
  ofstream f("rate_vs_temperature.txt");
  f << "Temperature, K;poole_frenkel;tunneling;total" << endl;
  
  for (double t = T_min; t < T_max; ++t) {
    RateResult r = rate_func(t, Efield, Eion);
    cout << "t = " << t << " rate: poole_frenkel = " << r.poole_frenkel << " tunneling = " << r.tunneling << " total = " << r.total() << endl;
    f << t << ";" << r.poole_frenkel << ";" << r.tunneling << ";" << r.total() << endl;
    
    gTotal->SetPoint(n, t, r.total());
    gPF->SetPoint(n, t, r.poole_frenkel);
    gT->SetPoint(n, t, r.tunneling);
    n++;
  }
  
  // plot rate vs. T
  TCanvas* c1 = new TCanvas("rate_vs_T", "rate_vs_T");
  c1->SetGrid();
  mg->Draw("APL");
  c1->BuildLegend(0.7, 0.75, 0.88, 0.88);
  
  
  // plot rate vs. 1/T
  TMultiGraph* mgIT = (TMultiGraph*) mg->Clone();
  mgIT->SetTitle("Emission rate;10^{3}/T, K^{-1};ln(rel_rate/T^{2}), K^{-2}");
  {
    TList* list = mgIT->GetListOfGraphs();
    for (int i = 0; i < list->GetSize(); ++i ) {
      TGraph* g = (TGraph*) list->At(i);
      const int xn = g->GetN();
      double* xdata = g->GetX();
      double* ydata = g->GetY();
      
      for (int j = 0; j < xn; ++j) {
        const double t = xdata[j];
        xdata[j] = 1e3/t;
        ydata[j] = log(ydata[j]/t/t);
      }
    }
  }
  
  TCanvas* c2 = new TCanvas("ln_rate_vs_T", "ln_rate_vs_T");
  c2->SetGrid();
  mgIT->Draw("APL");
  c2->BuildLegend(0.2, 0.75, 0.38, 0.88);
}

// forward
void rate_vs_efield(const double T, const vector<double> Efield, const double Eion);

// read Efield values from file ef_fname
void rate_vs_efield(const double T, const char* ef_fname, const double Eion)
{
  cout << "Reading Efield values from file " << ef_fname << endl;
  
  ifstream f(ef_fname);
  
  if (!f) {
    cout << "ERROR: failed to read input file " << ef_fname << endl;
    return;
  }
  
  vector<double> v;
  double x;
  while (f >> x) v.push_back(x);
  
  cout << "Total Efield points = " << v.size() << endl;
  
  rate_vs_efield(T, v, Eion);
}

// vary Efield in range Efield_min to Efield_max with logarithmic steps
void rate_vs_efield(const double T, const double Efield_min, const double Efield_max, const double Eion)
{
  vector<double> v;
  
  for (double x = Efield_min; x < Efield_max; x*=1.05)
    v.push_back(x);
  
  rate_vs_efield(T, v, Eion);
}


void rate_vs_efield(const double T, const vector<double> Efield, const double Eion)
{
  TGraph* gTotal = new TGraph;
  gTotal->SetTitle("total");
  gTotal->SetFillColor(0);
  gTotal->SetLineWidth(2);
  
  TGraph* gPF = new TGraph;
  gPF->SetTitle("poole_frenkel");
  gPF->SetFillColor(0);
  gPF->SetLineColor(kBlue);
  gPF->SetLineWidth(2);
  
  TGraph* gT = new TGraph;
  gT->SetTitle("tunneling");
  gT->SetFillColor(0);
  gT->SetLineColor(kRed);
  gT->SetLineWidth(2);
  
  TMultiGraph* mg = new TMultiGraph;
  mg->SetTitle("Emission rate;Efield, V/m;Relative rate");
  mg->Add(gTotal);
  mg->Add(gPF);
  mg->Add(gT);
  
  ofstream f("rate_vs_efield.txt");
  f << "Efield, V/m;poole_frenkel;tunneling;total" << endl;
  
  for (size_t n = 0; n < Efield.size(); ++n) {
    const double x = Efield[n];
    RateResult r = rate_func(T, x, Eion);
    cout << "Efield = " << x << " rate: poole_frenkel = " << r.poole_frenkel << " tunneling = " << r.tunneling << " total = " << r.total() << endl;
    f << x << ";" << r.poole_frenkel << ";" << r.tunneling << ";" << r.total() << endl;
    
    gTotal->SetPoint(n, x, r.total());
    gPF->SetPoint(n, x, r.poole_frenkel);
    gT->SetPoint(n, x, r.tunneling);
  }
  
  // plot rate vs. Efield
  TCanvas* c1 = new TCanvas("rate_vs_E", "rate_vs_E");
  c1->SetLogx();
  c1->SetLogy();
  c1->SetGrid();
  mg->Draw("APL");
  c1->BuildLegend(0.2, 0.75, 0.38, 0.88);
}

void rate()
{
  // temperature scan 200 - 350 K:
  rate_vs_temperature(200, 350, 2E7, 0.56);
  
  // electric field scan 1E6 - 1E9
  rate_vs_efield(300, 1E6, 1E9, 0.56);
  
  // a few single points
  rate_func(100, 1E7, 0.56).print();
  rate_func(350, 1E7, 0.56).print();
  rate_func(300, 2E7, 0.56).print();
  rate_func(300, 3E7, 0.56).print();
  rate_func(100, 1E8, 0.56).print();
  rate_func(100, 1E8, 0.80).print();
  rate_func(100, 1E7, 0.56).print();
}