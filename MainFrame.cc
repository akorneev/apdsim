#include <TRandom.h>
#include <TProfile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGraphErrors.h>
#include <Riostream.h>
#include <TCanvas.h>
#include <TGFrame.h>
#include <TGStatusBar.h>
#include <TGProgressBar.h>
#include <TGLabel.h>
#include <TGNumberEntry.h>
#include <TGTab.h>
#include <TGComboBox.h>
#include <TRootEmbeddedCanvas.h>
#include <TGFileDialog.h>
#include <TSystem.h>
#include <TLine.h>
#include <TStyle.h>
#include <TROOT.h>
  
#include "MainFrame.h"
#include "ApdModel.h"

ClassImp(MainFrame)

// create main window
MainFrame::MainFrame(const TGWindow* p, UInt_t w, UInt_t h)
: TGMainFrame(p, w, h)
{
  gStyle->SetOptStat("emrou");
  
  TGLayoutHints* hintsExpandXY = new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 2, 2, 2, 2);
  TGLayoutHints* hintsLabel = new TGLayoutHints(kLHintsCenterY, 2, 2, 2, 2);
  TGLayoutHints* hints = new TGLayoutHints(kLHintsNormal, 2, 2, 2, 2);
  
  TGHorizontalFrame* contentFrame = new TGHorizontalFrame(this);
  AddFrame(contentFrame, hintsExpandXY);
  
  TGVerticalFrame* frame1 = new TGVerticalFrame(contentFrame);
  contentFrame->AddFrame(frame1);
    
    TGGroupFrame* group1 = new TGGroupFrame(frame1, "Incident Light Model");
    frame1->AddFrame(group1, hints);
      
      group1->AddFrame(new TGLabel(group1, "Photon mean number = "), hintsLabel);
      fNmean = new TGNumberEntry(group1, 100, 7, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive);
      group1->AddFrame(fNmean, hints);
      
      group1->AddFrame(new TGLabel(group1, "Photon number distribution = "), hintsLabel);
      comboDistribution = new TGComboBox(group1);
      comboDistribution->AddEntry("Fixed", 0);
      comboDistribution->AddEntry("Poisson", 1);
      comboDistribution->Resize(100, 20);
      comboDistribution->Select(1);
      group1->AddFrame(comboDistribution, hints);
      
    TGGroupFrame* group2 = new TGGroupFrame(frame1, "APD Model");
    frame1->AddFrame(group2, hints);
      
      group2->AddFrame(new TGLabel(group2, "Nx = "), hintsLabel);
      fNx = new TGNumberEntry(group2, 30, 5, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive);
      group2->AddFrame(fNx, hints);
      
      group2->AddFrame(new TGLabel(group2, "Ny = "), hintsLabel);
      fNy = new TGNumberEntry(group2, 40, 5, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive);
      group2->AddFrame(fNy, hints);
      
      group2->AddFrame(new TGLabel(group2, "Crosstalk full probability = "), hintsLabel);
      entryCrossProb = new TGNumberEntry(group2, 0.24, 5, -1, TGNumberFormat::kNESReal, TGNumberFormat::kNEAAnyNumber, TGNumberFormat::kNELLimitMinMax, 0, 1);
      group2->AddFrame(entryCrossProb, hints);
    
      group2->AddFrame(new TGLabel(group2, "Crosstalk ratio (diagonal/cross) = "), hintsLabel);
      entryCrossRatio = new TGNumberEntry(group2, 1.0, 5, -1, TGNumberFormat::kNESReal, TGNumberFormat::kNEAPositive);
      group2->AddFrame(entryCrossRatio, hints);
      
    TGGroupFrame* group3 = new TGGroupFrame(frame1, "Simulation");
    frame1->AddFrame(group3, hints);
    
      group3->AddFrame(new TGLabel(group3, "Events number = "), hintsLabel);
      fNevt = new TGNumberEntry(group3, 100, 7, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive);
      group3->AddFrame(fNevt, hints);
      
      buttonSingle = new TGTextButton(group3, "RUN");
      buttonSingle->Connect("Clicked()", "MainFrame", this, "DoSingle()");
      group3->AddFrame(buttonSingle, hints);
    
    TGGroupFrame* group4 = new TGGroupFrame(frame1, "Parameter variation");
    frame1->AddFrame(group4, hints);
    
      group4->AddFrame(new TGLabel(group4, "Parameter = "), hintsLabel);
      TGComboBox* comboVariation = new TGComboBox(group4);
      comboVariation->AddEntry("Photon mean number", 0);
      //comboVariation->Layout();
      comboVariation->Resize(150, 20);
      comboVariation->Select(0);
      group4->AddFrame(comboVariation, hints);
      
      group4->AddFrame(new TGLabel(group4, "From = "), hintsLabel);
      entryFrom = new TGNumberEntry(group4, 0, 7, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive);
      //entryFrom->SetState(kFALSE);
      group4->AddFrame(entryFrom, hints);
      
      group4->AddFrame(new TGLabel(group4, "To = "), hintsLabel);
      entryTo = new TGNumberEntry(group4, 100, 7, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive);
      group4->AddFrame(entryTo, hints);
      
      group4->AddFrame(new TGLabel(group4, "Step = "), hintsLabel);
      entryStep = new TGNumberEntry(group4, 10, 6, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive);
      group4->AddFrame(entryStep, hints);
    
      buttonAll = new TGTextButton(group4, "RUN");
      buttonAll->Connect("Clicked()", "MainFrame", this, "DoAll()");
      group4->AddFrame(buttonAll, hints);
    
    TGButton* save = new TGTextButton(frame1, "SAVE");
    save->Connect("Clicked()", "MainFrame", this, "DoSave()");
    frame1->AddFrame(save, hints);
  
  // tabs
  TGTab* pTab = new TGTab(contentFrame);
  contentFrame->AddFrame(pTab, hintsExpandXY);
  
  TGCompositeFrame* pTab1 = pTab->AddTab("Disp1");
  
    TRootEmbeddedCanvas* pC1 = new TRootEmbeddedCanvas("disp1", pTab1);
    pTab1->AddFrame(pC1, hintsExpandXY);
    fCan1 = pC1->GetCanvas();
  
  TGCompositeFrame* pTab2 = pTab->AddTab("Disp2");
    TRootEmbeddedCanvas* pC2 = new TRootEmbeddedCanvas("disp2", pTab2);
    pTab2->AddFrame(pC2, hintsExpandXY);
    fCan2 = pC2->GetCanvas();
  
  // status bar
  statusBar = new TGStatusBar(this);
  AddFrame(statusBar, new TGLayoutHints(kLHintsExpandX, 0, 0, 2));
  
  // common part
  SetCleanup(kDeepCleanup);
  SetWindowName(gROOT->GetName());
  
  // arrange widgets
  MapSubwindows();
  MapWindow();
  Layout();
}

void MainFrame::DoSingle()
{
  const Int_t nmean = fNmean->GetIntNumber();
  const Int_t distr = comboDistribution->GetSelected();
  const Int_t nx = fNx->GetIntNumber();
  const Int_t ny = fNy->GetIntNumber();
  const Float_t prob = entryCrossProb->GetNumber();
  const Float_t probr = entryCrossRatio->GetNumber();
  
  buttonSingle->SetState(kButtonDisabled);
  gSystem->ProcessEvents();
  
  TH1F* h1 = (TH1F*) gROOT->Get("sst");
  if (h1) delete h1;
  h1 = new TH1F("sst", "Number of pixels fired;number of pixels fired;#events", nx*ny + 1, 0, nx*ny + 1);
  
  TH2F* h2 = (TH2F*) gROOT->Get("map");
  if (h2) delete h2;
  h2 = new TH2F("map", "Fired pixels map;X, pixel;Y, pixel", nx, 0, nx, ny, 0, ny);
  
  Apd apd(nx, ny, prob, probr);
  
  for (Int_t i = 0; i < fNevt->GetIntNumber(); i++) {
    const Int_t n = distr ? gRandom->Poisson(nmean) : nmean;
    h1->Fill(apd.SampleEvent(n));
    apd.FillMap(h2);
  }
  
  h1->SetTitle(Form("Mean N. photons %i Mean= %.2f RMS=%.2f", nmean, h1->GetMean(), h1->GetRMS()));
  h1->SetAxisRange(h1->GetMean() - 5*h1->GetRMS(), h1->GetMean() + 5*h1->GetRMS());
  
  fCan1->cd();
  h1->Draw();
  fCan1->Update();
  
  fCan2->cd();
  h2->Draw("LEGO");
  fCan2->Update();
  
  buttonSingle->SetState(kButtonUp);
}

void MainFrame::DoSave()
{
  const char* fileTypes[] = {"Text file", "*.txt", 0};
  TGFileInfo fi;
  fi.fFileTypes = fileTypes;
  new TGFileDialog(fClient->GetRoot(), this, kFDSave, &fi);
  if (!fi.fFilename) return;
  
  TH1F* h1 = (TH1F*) gROOT->Get("sst");
  if(!h1) return;
  
  ofstream file(fi.fFilename);
  
  for (Int_t i = 1; i <= h1->GetNbinsX(); ++i)
    file << Form(" %4i ; %8.2f ; %8.2f\n", i, h1->GetBinCenter(i), h1->GetBinContent(i));
  
  setStatus(Form("Saved to file %s", fi.fFilename));
}

void MainFrame::DoAll()
{
  const Int_t distr = comboDistribution->GetSelected();
  const Int_t from = entryFrom->GetIntNumber();
  const Int_t to = entryTo->GetIntNumber();
  const Int_t step = entryStep->GetIntNumber();
  const Int_t nx = fNx->GetIntNumber();
  const Int_t ny = fNy->GetIntNumber();
  const Float_t prob = entryCrossProb->GetNumber();
  const Float_t probr = entryCrossRatio->GetNumber();
  
  setStatus(Form("Doing Nmean from 1 to %i step %i...", to, step));
  buttonAll->SetState(kButtonDisabled);
  gSystem->ProcessEvents();
  
  TProfile* h0 = new TProfile("sstmp", "", 1, 0, 1, "S");
  TProfile* h1 = new TProfile("sstmf", "", 1, 0, 1, "S");
  
  TGraphErrors* g0 = new TGraphErrors;
  g0->SetNameTitle("response", "SiPM Response;incident photons;pixels fired");
  g0->SetMarkerStyle(kFullTriangleUp);
  g0->SetMarkerColor(kBlue);
  g0->SetLineColor(kBlue);
  
  TGraphErrors* g1 = new TGraphErrors;
  g0->SetName("response-crosstalk");
  g1->SetMarkerStyle(kFullTriangleDown);
  g1->SetMarkerColor(kRed);
  g1->SetLineColor(kRed);
  
  Apd apd0(nx, ny, 0, 1);
  Apd apd1(nx, ny, prob, probr);
  
  ofstream file("MAIN.txt");
  
  Int_t point = 0;
  
  for (Int_t i = from; i <= to; i += step) {
    // avoid zero
    const Int_t nmean = (i) ? i : 1;
    
    for (Int_t i = 0; i < fNevt->GetIntNumber(); i++) {
      const Int_t n = distr ? gRandom->Poisson(nmean) : nmean;
      h0->Fill(0., apd0.SampleEvent(n));
      h1->Fill(0., apd1.SampleEvent(n));
    }
    
    g0->SetPoint(point, nmean, h0->GetBinContent(1));
    g0->SetPointError(point, 0, h0->GetBinError(1));
    g1->SetPoint(point, nmean, h1->GetBinContent(1)); // mean
    g1->SetPointError(point, 0, h1->GetBinError(1));  // rms
    point++;
    
    file << Form("Nmean=%4i ; Nsamp=%8.3f ; RMS=%8.3f\n", nmean, h1->GetBinContent(1), h1->GetBinError(1));
    h0->Reset();
    h1->Reset();
    
    setStatus(Form("Doing Nmean %i of %i ...", nmean, to));
  }
  
  delete h0;
  delete h1;
  
  TLine* l = new TLine(5, 5, to, to);
  l->SetLineColor(kGray);
  
  fCan1->cd();
  fCan1->SetGrid();
  g0->Draw("apl");
  g1->Draw("pl");
  l->Draw();
  fCan1->Update();
  
  fCan2->Clear();
  fCan2->Update();
  
  buttonAll->SetState(kButtonUp);
  setStatus(Form("Done Nmean from 0 to %i step %i", to, step));
}

void MainFrame::redraw(TGWindow* window)
{
  gClient->NeedRedraw(window);
  gSystem->ProcessEvents();
}

void MainFrame::setStatus(const char* status)
{
  statusBar->SetText(status);
  redraw(statusBar);
}

void apd()
{
  new MainFrame(0, 800, 700);
}
