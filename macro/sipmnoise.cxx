#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TGraph2D.h"
#include "TMultiGraph.h"
#include "TH2.h"
#include "TGraph2DErrors.h"
#include "TStopwatch.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"
#include "TF1.h"
#include "TH1.h"
#include "TRandom3.h"
#include "TPRegexp.h"
#include "TLine.h"
//#include "Math/IntegratorOptions.h"

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#include <map>
using namespace std;

//#include <omp.h>

//#include <windows.h>

void zoom_to_xcontents(TH1* h)
{
  const int f = h->FindFirstBinAbove(0);
  const int l = h->FindLastBinAbove(0);
  TAxis* a = h->GetXaxis();
  a->SetRange(f-10, l+10);
}

TCanvas* make_canvas(const char* name)
{
  // no canvas in batch mode
  if (gROOT->IsBatch()) return 0;
  
  TCanvas* c = (TCanvas*) gROOT->GetListOfCanvases()->FindObject(name);
  if (!c) {
    c = new TCanvas(name, name);
    c->SetGrid();
  }
  c->Clear();
  c->cd();
  
  return c;
}

void th1_save_csv(const string fcsv, TH1* h)
{
  cout << "INFO: output file " << fcsv << " for histogram " << h->GetName() << " / " << h->GetTitle() << endl;
  
  ofstream f(fcsv.c_str());
  
  f << "low edge;up edge;content;error;" << endl;
  
  for (int i = 1; i <= h->GetNbinsX(); ++i) {
    f << h->GetXaxis()->GetBinLowEdge(i) << ";"
      << h->GetYaxis()->GetBinUpEdge(i) << ";"
      << h->GetBinContent(i) << ";"
      << h->GetBinError(i) << ";"
      //<< h->GetBinEntries(i) << ";"
      << endl;
  }
}

double sqr(const double x) { return x*x; }

// convert string to value
template<typename T>
T cast(const std::string& s)
{
  std::istringstream iss(s);
  T x;
  iss >> x;
  return x;
}

// convert value to string
template<typename T>
string tostring(const T& x)
{
  std::ostringstream oss;
  oss << x;
  return oss.str();
}


struct SipmParams {
  double Tpre;       // pre time window, ns
  double Tint;       // integration time window, ns
  string crosstalk_model; // crosstalk model: geometric, borel
  double crosstalk_param;  // crosstalk probability
  string pulseshape; // single electron pulse shape: exp, expexp, delta
  double tau1;       // pulse shape parameters (time [ns], weight)
  double w1;         //   exp: tau1
  double tau2;       //   expexp: tau1, w1, tau2
  double fnoise;     // noise frequency, GHz
  Int_t stat;        // number of events
  Int_t seed;        // rnd seed
  
  
  void set(const string name, const string value)
  {
    if (name == "crosstalk_model") {
      const bool isValid = (value == "geometric" || value == "borel");
      if (!isValid) {
        // throw
        cout << "ERROR: SipmParams::set() unknown parameter value: " + name << "=" << value << endl;
      }
      
      crosstalk_model = value;
      return;
    }
    
    if (name == "pulseshape") {
      const bool isValid = (value == "exp" || value == "expexp" || value == "delta");
      if (!isValid) {
        // throw
        cout << "ERROR: SipmParams::set() unknown parameter value: " + name << "=" << value << endl;
      }
      
      pulseshape = value;
      return;
    }
    
    if (name == "stat")            { stat = cast<int>(value); return; }
    if (name == "seed")            { seed = cast<int>(value); return; }
    
    setdouble(name, cast<double>(value));
  }
  
  void setdouble(const string name, const double value)
  {
    if (name == "Tpre")      { Tpre = value;       return; }
    if (name == "Tint")      { Tint = value;       return; }
    if (name == "crosstalk_param") { crosstalk_param = value;  return; }
    if (name == "X_talk")     { crosstalk_param = -log(1 - value);  return; }
    if (name == "tau1")      { tau1 = value;       return; }
    if (name == "w1")        { w1 = value;         return; }
    if (name == "tau2")      { tau2 = value;       return; }
    if (name == "fnoise")    { fnoise = value;     return; }
    
    //throw std::runtime_error("SipmParams::set() unknown parameter name " + name);
    cout << "ERROR: SipmParams::set() unknown parameter name " + name << endl;
  }
  
  double get(const string name) const
  {
    if (name == "Tpre")      return Tpre;
    if (name == "Tint")      return Tint;
    if (name == "crosstalk_param") return crosstalk_param;
    if (name == "X_talk")     return 1 - exp(-crosstalk_param);
    if (name == "tau1")      return tau1;
    if (name == "w1")        return w1;
    if (name == "tau2")      return tau2;
    if (name == "fnoise")    return fnoise;
    
    //throw std::runtime_error("SipmParams::get() unknown parameter name " + name);
    cout << "ERROR: SipmParams::get() unknown parameter name " + name << endl;
    return 0;
  }
  
  string csv_head() const
  {
    return "Tpre;Tint;crosstalk_model;crosstalk_param;X_talk;pulseshape;tau1;w1;tau2;fnoise;stat;seed";
  }
  
  string csv() const
  {
    ostringstream oss;
    oss << Tpre << ";"
        << Tint << ";"
        << crosstalk_model << ";"
        << crosstalk_param << ";"
        << get("X_talk") << ";"
        << pulseshape << ";"
        << tau1 << ";"
        << w1 << ";"
        << tau2 << ";"
        << fnoise << ";"
        << stat << ";"
        << seed;
    return oss.str();
  }
};

struct SipmResults {
  double NmeanTfull;
  double seTint;
  
  double int_mean;
  double int_meanerr;
  double int_rms;
  double int_rmserr;
  
  double int_mean_norm() const { return int_mean/seTint; }
  //double int_rms_norm() const { return int_rms/seTint; }
  //double int_rmserr_norm() const { return int_rmserr/seTint; }
  
  // equivalent noise charge (= input charge for which S/N=1)
  double ENC() const { return int_rms/seTint; }
  double ENCerr() const { return int_rmserr/seTint; }
  
  
  double pixels_fired_mean;
  double pixels_fired_rms;
  
  double crosstalk_total;
  
  // excess noise factor
  double ENF() const { return 1 + sqr(pixels_fired_rms) / sqr(pixels_fired_mean); }
  
  void summary() const {
    cout << "===> Summary:" << endl;
    
    cout << "mean number of noise pulses in window [-Tpre, Tint] nmean = " << NmeanTfull << endl;
    cout << "integral of single electron pulse in [0, Tint] norm = " << seTint << endl;
    
    cout << "hsum: mean = " << int_mean << " (error = " << int_meanerr << ")" << endl;
    cout << "hsum: rms = " << int_rms << " (error = " << int_rmserr << ")" << endl;
    cout << "hsum: mean/norm = " << int_mean_norm() << endl;
    cout << "hsum: ENC = rms/norm = " << ENC() << " (error = " << ENCerr() << ")" << endl;
    
    cout << "pixels fired mean = " << pixels_fired_mean << endl;
    cout << "pixels fired rms = " << pixels_fired_rms << endl;
    cout << "ENF = " << ENF() << endl;
    cout << "crosstalk total = " << crosstalk_total << endl;
  }
  
  string csv_head() const
  {
    return "NmeanTfull;seTint;int_mean;int_meanerr;int_rms;int_rmserr;int_mean_norm;ENC;ENCerr;pixels_fired_mean;pixels_fired_rms;ENF;crosstalk_total;";
  }
  
  string csv() const
  {
    ostringstream oss;
    oss << NmeanTfull << ";"
        << seTint << ";"
        << int_mean << ";"
        << int_meanerr << ";"
        << int_rms << ";"
        << int_rmserr << ";"
        << int_mean_norm() << ";"
        << ENC() << ";"
        << ENCerr() << ";"
        << pixels_fired_mean << ";"
        << pixels_fired_rms << ";"
        << ENF() << ";"
        << crosstalk_total << ";"
        ;
    return oss.str();
  }
};

int geometric(TRandom* rnd, double mean)
{
  int tot = 0;
  while (rnd->Uniform() < mean) tot++;
  return tot;
}

int borel(TRandom* rnd, double mean)
{
  //cout << "borel(): mean=" << mean << endl;
  
  int tot = 0;
  
  int nnew = 1;
  while (nnew > 0) {
    const int next = rnd->Poisson(mean);
    //cout << "borel(): next=" << next << endl;
    nnew -= 1;
    
    nnew += next;
    tot += next;
  }
  
  return tot;
}

SipmResults sipmnoise(const SipmParams p)
{
  gStyle->SetLineScalePS(1);
  gStyle->SetOptFit(112);
  gStyle->SetOptStat("emruo");
  //gStyle->SetOptFit(1111);
  gStyle->SetGridColor(kGray);
  //gStyle->SetGridStyle(7);
  gStyle->SetGridWidth(1);
  TH1::SetDefaultSumw2(kTRUE);
  TH1::StatOverflows(kTRUE);
  TH1::AddDirectory(kFALSE);
  
  const double epsrel = 1.E-6;
  //ROOT::Math::IntegratorOneDimOptions::SetDefaultRelTolerance(epsrel);
  
  const bool isGUI = !gROOT->IsBatch();
  
  // single electron pulse shape 
  TF1* fpixwfm = 0;
  
  // include seed into waveform name to avoid possible multi-threading issues
  // (internaly TF1 add each function to global list `gROOT->GetListOfFunctions()->Add(this);`)
  
  TString wfmname;
  wfmname.Format("fpixwfm%d", p.seed);
  
  if (p.pulseshape == "delta") {
    // immitate delta function with narrow gaus for visualisation purpose only
    // integration is handled manually
    #pragma omp critical
    fpixwfm = new TF1(wfmname, "[1]*exp(-0.5*((x-[0])/[2])**2)", -p.Tpre, p.Tint);
    fpixwfm->SetParameter(0, 0.);
    fpixwfm->SetParameter(1, 1.);
    fpixwfm->SetParameter(2, (p.Tpre + p.Tint)/1000.);
  }
  
  if (p.pulseshape == "exp") {
    #pragma omp critical
    fpixwfm = new TF1(wfmname, "(x>=[0])*[1]*exp(-(x-[0])/[2])/[2]", -p.Tpre, p.Tint);
    fpixwfm->SetParameter(0, 0.);
    fpixwfm->SetParameter(1, 1.);
    fpixwfm->SetParameter(2, p.tau1);
  }
  
  if (p.pulseshape == "expexp") {
    #pragma omp critical
    fpixwfm = new TF1(wfmname, "(x>=[0])*[1]*([3]*exp(-(x-[0])/[2])/[2] + (1-[3])*exp(-(x-[0])/[4])/[4])", -p.Tpre, p.Tint);
    fpixwfm->SetParameter(0, 0.);
    fpixwfm->SetParameter(1, 1.);
    fpixwfm->SetParameter(2, p.tau1);
    fpixwfm->SetParameter(3, p.w1);
    fpixwfm->SetParameter(4, p.tau2);
  }
  
  fpixwfm->SetLineWidth(1);
  fpixwfm->SetNpx(1000);
  
  if (isGUI) {
    TCanvas* c = make_canvas("cwfm");
    //c->ToggleEventStatus();
    
    /*
    if (p.pulseshape == "delta") {
      TLine l;
      l.PaintLine(0, 0, 0, 1.);
    }
    else
    */
    TF1* f = fpixwfm->DrawCopy();
    f->GetHistogram()->SetTitle("Single electron pulse shape (" + TString(p.pulseshape) + ");time, ns;amplitude");
    c->Update();
  }
  
  // mean number of noise events
  const double NmeanTfull = (p.Tpre+p.Tint) * p.fnoise;
  const double seTint = (p.pulseshape == "delta") ? 1. :
  #if ROOT_VERSION_CODE >= ROOT_VERSION(6,0,0)
    fpixwfm->Integral(0, p.Tint, epsrel);
  #else
    fpixwfm->Integral(0, p.Tint);
  #endif
  
  TH1I* hnactual = new TH1I("hnactual", "Actual number of noise pulses in [-Tpre, Tint]", 1000, 0, 1000);
  TH1I* hsum = new TH1I("hsum", "Integral of all noise pulses in [0, Tint]", 1000, 0, 1000);
  TH1I* ht0 = new TH1I("ht0", "Individual noise pulse start time;t0, ns", (p.Tpre+p.Tint), -p.Tpre, p.Tint);
  //TH1I* hdt0 = new TH1I("hdt0", "Noise pulse-to-pulse interval time", 100, 0, 5/fnoise);
  TH1D* hcross = new TH1D("hcross", "Crosstalk;pixels fired per single electron pulse", 1000, 0, 1000);
  
  if (isGUI) {
    TCanvas* c0 = make_canvas("cwfm1event");
    const double maxA = fpixwfm->Eval(0);
    c0->DrawFrame(-p.Tpre, 0, p.Tint, 3.1*maxA, "Noise waveforms;time, ns;amplitude");
  }
  
  TRandom* rnd = new TRandom3(p.seed);
  
  for (Int_t m = 0; m < p.stat; m++) {
    // actual number of noise events
    const Int_t nactual = rnd->Poisson(NmeanTfull);
    hnactual->Fill(nactual);
    //cout << "nactual = " << nactual << endl;
    
    double Sum = 0;
    //vector<double> t0s(nactual, 0);
    
    for (int i = 0; i < nactual; ++i) {
      const double t0 = rnd->Uniform(-p.Tpre, p.Tint);
      //cout << "t0=" << t0 << endl;
      //t0s[i] = t0;
      ht0->Fill(t0);
      fpixwfm->SetParameter(0, t0);
      
      // calculate crosstalk
      int A = 1;
      
      if (p.crosstalk_model == "geometric") {
        A += geometric(rnd, p.crosstalk_param);
      }
      
      if (p.crosstalk_model == "borel") {
        A += borel(rnd, p.crosstalk_param);
      }
      
      hcross->Fill(A);
      fpixwfm->SetParameter(1, A);
      
      if (isGUI)
        if (m == 0) {
          /*
          if (p.pulseshape == "delta") {
            TLine l;
            l.PaintLine(t0, 0, t0, A);
          }
          else
          */
          fpixwfm->DrawCopy("same");
        }
      
      const Double_t s = (p.pulseshape == "delta") ? ((0 <= t0 && t0 < p.Tint) ? A : 0) :
      #if ROOT_VERSION_CODE >= ROOT_VERSION(6,0,0)
        fpixwfm->Integral(0, p.Tint, epsrel);
      #else
        fpixwfm->Integral(0, p.Tint);
      #endif
      
      //cout << "s=" << s << endl;
      Sum += s;
    }
    
    
    hsum->Fill(Sum);
    //cout << "Sum = " << Sum << endl;
    
    //sort(t0s.begin(), t0s.end());
    
    //for (size_t j = 1; j < t0s.size(); ++j)
    //  hdt0.Fill(t0s[j] - t0s[j-1]);
  }
  
  
  const SipmResults r = {
    NmeanTfull, seTint,
    hsum->GetMean(), hsum->GetMeanError(), hsum->GetRMS(), hsum->GetRMSError(),
    hcross->GetMean(), hcross->GetRMS(),
    1. - hcross->GetBinContent(2) / hcross->GetEntries()
  };
  
  //r.summary();
  //cout << endl;
  
  // NOTE: all TH1::GetMean() calls should be before zoom_to_xcontents() to avoid binning effects due to SetRange call
  
  if (isGUI) {
    make_canvas("cnactual");
    hnactual->Draw();
    zoom_to_xcontents(hnactual);
    
    make_canvas("ct0");
    ht0->Draw();
    zoom_to_xcontents(ht0);
    
    //make_canvas("ct0delta");
    //hdt0->Draw();
    //zoom_to_xcontents(ht0d);
    
    TCanvas* c3 = make_canvas("ccross");
    c3->SetLogy();
    hcross->Scale(1./hcross->GetEntries());
    hcross->Draw();
    zoom_to_xcontents(hcross);
    th1_save_csv("hcross.txt", hcross);
    
    make_canvas("csum");
    hsum->Draw();
    zoom_to_xcontents(hsum);
    
    /*
    pixh->Fit("gaus");
    TF1* fitf = pixh->GetFunction("gaus");
    fitf->SetNpx(1000);
    */
  }
  
  return r;
}

struct SipmRun {
  SipmParams x;
  SipmResults y;
};

void save(string fname, SipmRun* runs, int n)
{
  cout << "INFO: save batch output to " << fname << endl;
  
  ofstream f(fname.c_str());
  
  f << runs[0].x.csv_head() << ";" << runs[0].y.csv_head() << endl;
  
  for (int i = 0; i < n; ++i) {
    f << runs[i].x.csv() << ";" << runs[i].y.csv() << endl;
  }
}

struct ParamRange {
  string name;   // parameter name
  double start;  // loop range = start / stop / step
  double stop;
  double step;
  
  bool isFixed() const { return (start + step) > stop; }
  
  TString title() const {
    TString s;
    s.Form("%s=[%s-%s/%s]",
           name.c_str(),
           tostring(start).c_str(),
           tostring(stop).c_str(),
           tostring(step).c_str());
    return s;
  }
};



TMultiGraph* graph2d_to_multigraph(TGraph2DErrors* g)
{
  // enumerate dimensions
  const int gn = g->GetN();
  double* gx = g->GetX();
  double* gy = g->GetY();
  double* gz = g->GetZ();
  //double* ex = g->GetEX();
  //double* ey = g->GetEY();
  double* gez = g->GetEZ();
  map<double, bool> ax;
  map<double, bool> ay;
  
  for (int j = 0; j < gn; ++j) {
    ax[gx[j]] = true;
    ay[gy[j]] = true;
  }
  
  const int nx = ax.size();
  const int ny = ay.size();
  const int nymax = 10000;
  
  TMultiGraph* mg = new TMultiGraph;
  
  
  TString title = g->GetTitle();
  TPRegexp("^([^;]+).*").Substitute(title, "$1");
  
  mg->SetTitle(title + ";" + TString(g->GetYaxis()->GetTitle()) + ";" + TString(g->GetZaxis()->GetTitle()));
  
  int idx = 0;
  int color = 1; // start from black
  
  for (size_t ix = 0; ix < nx; ++ix) {
    TString name;
    name.Form("%s=%s", g->GetXaxis()->GetTitle(), tostring(gx[idx]).c_str());
    
    double x[nymax];
    double y[nymax];
    double ex[nymax];
    double ey[nymax];
    
    for (size_t iy = 0; iy < ny; ++iy) {
      x[iy] = gy[idx];
      y[iy] = gz[idx];
      ex[iy] = 0.;
      ey[iy] = gez[idx];
      idx++;
    }
    
    TGraphErrors* g = new TGraphErrors(ny, x, y, ex, ey);
    g->SetTitle(name);
    g->SetMarkerStyle(20 + ix);
    g->SetMarkerSize(0.5);
    g->SetFillColor(0);
    g->SetMarkerColor(color);
    g->SetLineColor(color);
    mg->Add(g);
    
    color++;
    if (color == 5) color++; // skip yellow
    if (color == 10) color = 28; // skip gray and brown variations
  }
  
  //cout << "INFO: graph2d_to_multigraph() nplots=" << mg->GetListOfGraphs()->GetSize() << endl;
  
  return mg;
}


void sipmnoisebatch(
  SipmParams p,
  const ParamRange r1,
  const ParamRange r2
)
{
#if ROOT_VERSION_CODE >= ROOT_VERSION(6,0,0)
  ROOT::EnableThreadSafety();
#endif
  
  // NOTE: vector<> bad support by root5 CINT
  const int nmax = 10000;
  SipmRun* runs = new SipmRun[nmax];
  int n = 0;
  
  // generate runs
  for (double ix = r1.start; ix < r1.stop; ix += r1.step) {
  for (double iy = r2.start; iy < r2.stop; iy += r2.step) {
    //cout << xparam << " = " << ix << " " << yparam << " = " << iy << endl;
    p.setdouble(r1.name, ix);
    p.setdouble(r2.name, iy);
    
    runs[n].x = p;
    p.seed++;
    n++;
  }
  }
  
  const bool isBatch = gROOT->IsBatch();
  gROOT->SetBatch(kTRUE);
  
  #pragma omp parallel for
  for (int i = 0; i < n; ++i) {
    SipmRun& r = runs[i];
    #pragma omp critical
    cout << "run #" << (i+1) << " of " << n << ": "
         << r1.name << " = " << r.x.get(r1.name) << " "
         << r2.name << " = " << r.x.get(r2.name) << " "
         << "seed = " << r.x.seed
         << endl;
    
    r.y = sipmnoise(r.x);
  }
  
  gROOT->SetBatch(isBatch);
  
  save("sipmnoisebatch.txt", runs, n);
  
  const bool is1D = r2.isFixed();
  //cout << "INFO: running " << (is1D ? "1D" : "2D") << " mode" << endl;
  
  if (is1D) {
  TGraphErrors* g = new TGraphErrors;
  TString title;
  title.Form("ENC %s;%s;ENC", r1.title().Data(), r1.name.c_str());
  g->SetTitle(title);
  g->SetMarkerStyle(kFullDotMedium);
  
  for (int j = 0; j < n; ++j) {
    g->SetPoint(j, runs[j].x.get(r1.name), runs[j].y.ENC());
    g->SetPointError(j, 0, runs[j].y.ENCerr());
  }
  
  TCanvas* c = new TCanvas("c_enc_1d", "c_enc_1d");
  c->SetGrid();
  g->Draw("ALP");
  }
  
  if (!is1D) {
  TGraph2DErrors* g = new TGraph2DErrors;
  TString title;
  title.Form("ENC %s %s;%s;%s;ENC", r1.title().Data(), r2.title().Data(), r1.name.c_str(), r2.name.c_str());
  g->SetTitle(title);
  g->SetMarkerStyle(kFullCircle);
  g->SetMarkerSize(0.5);
  g->SetLineStyle(2);
  
  for (int j = 0; j < n; ++j) {
    g->SetPoint(j, runs[j].x.get(r1.name), runs[j].x.get(r2.name), runs[j].y.ENC());
    g->SetPointError(j, 0, 0, runs[j].y.ENCerr());
  }
  
  TCanvas* c = new TCanvas("c_enc_2d", "c_enc_2d");
  c->SetGrid();
  g->Draw("tri1 p err");
  g->GetHistogram()->GetXaxis()->SetTitleOffset(1.5);
  g->GetHistogram()->GetYaxis()->SetTitleOffset(1.5);
  
  TMultiGraph* mg = graph2d_to_multigraph(g);
  TCanvas* c1 = new TCanvas("c_enc_2d_slice", "c_enc_2d_slice");
  c1->SetGrid();
  mg->Draw("APL");
  c1->BuildLegend();
  }
  
  delete [] runs;
}

string trim(string s)
{
  // prefixing spaces
  s.erase(0, s.find_first_not_of(' '));
  
  // surfixing spaces
  s.erase(s.find_last_not_of(' ') + 1);
  
  return s;
}

vector<string> explode(const char delimiter, const string input)
{
  vector<string> result;
  
  istringstream buffer(input);
  string item;
  
  while (std::getline(buffer, item, delimiter))
    if (!item.empty())
      result.push_back(item);
  
  return result;
}

void run_config(const string fconfig = "sipmnoiseinput.txt")
{
  ifstream f(fconfig.c_str());
  
  if (!f) {
    //throw std::runtime_error("ERROR: run_config() file not exists or IO error " + fconfig);
    
    cout << "ERROR: run_config() file not exists or IO error " + fconfig << endl;
    return;
  }
  
  SipmParams p;
  string line;
  
  /*
  // default
  SipmParams p = {
    100,      // pre time window, ns
    10,       // integration time window, ns
    "borel",  // crosstalk model: geometric, borel
    0.1,      // crosstalk probability
    "expexp", // noise pulse shape: exp, expexp
    1,        // pulse shape parameters (time [ns], weight)
    0.1,      //   exp: tau1
    15,       //   expexp: tau1, w1, tau2
    1,        // noise frequency, GHz
    100      // number of events
  };
  */
  
  while (getline(f, line)) {
    // remove comments
    const size_t grillpos = line.find("#");
    if (grillpos != string::npos) {
      line = line.substr(0, grillpos);
    }
    
    // trim spaces
    line = trim(line);
    
    // skip empty lines
    if (line == "") continue;
    
    // key=value section
    const size_t eqpos = line.find("=");
    if (eqpos != string::npos) {
      // if where is '=' sign in line, this is a property string: key=value
      const string key = trim(line.substr(0, eqpos));
      const string value = trim(line.substr(eqpos + 1));
      
      p.set(key, value);
      continue;
    }
    
    // command section
    const vector<string> a = explode(' ', line);
    
    if (a[0] == "run") {
      const SipmResults r = sipmnoise(p);
      r.summary();
      cout << endl;
    }
    
    if (a[0] == "batch") {
      // run timer
      TStopwatch t;
      t.Start();
      
      sipmnoisebatch(
        p,
        {a[1], cast<double>(a[2]), cast<double>(a[3]), cast<double>(a[4])},
        {a[5], cast<double>(a[6]), cast<double>(a[7]), cast<double>(a[8])}
      );
      
      // print elapsed time
      t.Stop();
      t.Print();
    }
  }
}


int main(int argc, char* argv[])
{
  TApplication* app = new TApplication("app", &argc, argv);
  TQObject::Connect("TCanvas", "Closed()", "TApplication", app, "Terminate()");
  
  run_config();
  
  //cout << "batch=" << gROOT->IsBatch() << endl;
  const bool isBatch = gROOT->IsBatch();
  if (!isBatch)
    app->Run(kTRUE);
  
  return 0;
}
