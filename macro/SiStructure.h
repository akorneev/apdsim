#include "TF1.h"
#include "TGraph.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
using namespace std;


void save_data(TGraph* g, const char* fname)
{
  cout << "save_data() obj=" << g->GetName() << " fname=" << fname << endl;
  
  ofstream f(fname);
  
  const Int_t N = g->GetN();
  const Double_t* x = g->GetX();
  const Double_t* y = g->GetY();
  
  for (Int_t i = 0; i < N; i++) {
    f << x[i] << ";" << y[i] << "\n";
  }
}

void save_data(TF1* f, const char* fname)
{
  TGraph* g = new TGraph(f);
  save_data(g, fname);
}


struct SiLayer {
  string name;
  double N0;       // cm^-3
  double sigma;    // um
  double center;   // um
};

struct SiRange {
  double min;  // um
  double max;  // um
  
  double size() const { return (max - min); }
};

struct SiStructure {
  static const Int_t po = 3000;
  
  //vector<SiLayer> structure;
  
  Double_t Nppl, Np, Nn, Npi, Nnpl;
  Double_t sppl, sp, sn,      snpl;
  Double_t totdepth;
  
  TF1* fpr;
  TF1* f1;
  TF1* f1depl;
  TF1* fa;
  TF1* fb;
  TF1* fab;
  
  bool load(const char* fname);
  void print() const;
  string str() const;
  
  // dopping profile
  TF1* getDoppingProfile() const { return fpr; };
  
  // electric field
  TF1* getElectricFieldRaw() const { return f1; };
  TF1* getElectricField() const { return f1depl; };
  
  // update electric field according to bias voltage
  // return actual voltage reached
  double setBias(const double voltage);
  
  // compute depletion regions
  vector<SiRange> depl;
  void calcDepletion();
  double totalDepl() const;
  
  // compute gain vs depth
  TGraph* gM;
  void calcGain();
  
  double alpha_vs_efield(const double e) const;
  double beta_vs_efield(const double e) const;
  
  Double_t alpha_vs_depth(Double_t* x, Double_t* par);
  Double_t beta_vs_depth(Double_t* x, Double_t* par);
  Double_t alphabeta_vs_depth(Double_t* x, Double_t* par);
};

bool SiStructure::load(const char* fname)
{
  cout << "SiStructure::load() fname = " << fname << endl;
  
  ifstream fin(fname);
  
  fin >> Nppl;
  fin >> Np;
  fin >> Nn;
  fin >> Npi;
  fin >> Nnpl;
  
  fin >> sppl;
  fin >> sp;
  fin >> sn;
  fin >> snpl;
  
  fin >> totdepth;
  
  if (!fin) {
    cout << "ERROR: input file format error" << endl;
    return false;
  }
  
  // dopping profile
  fpr = new TF1("fpr", "abs([0]*exp(-x**2/([5]**2)) + [1]*exp(-x**2/([6]**2)) + [2]*exp(-x**2/([7]**2)) + [3] + [4]*exp((-(x-[9])**2)/([8]**2)))", 0, totdepth);
  fpr->SetParameters(Nppl,Np,Nn,Npi,Nnpl,sppl,sp,sn,snpl,totdepth);
  fpr->SetNpx(po);
  
  // electric field is derived from:
  //
  // dE(x) / dx = rho(x) / eps
  // rho(x) = q * dopping(x)
  f1 = new TF1("f1", "13.5e-12 * ([0]*[5]*TMath::Erf(x/[5]) + [1]*[6]*TMath::Erf(x/[6]) + [2]*[7]*TMath::Erf(x/[7]) + [3]*x - [4]*[8]*TMath::Erf(([9]-x)/[8])) - [10]", 0, totdepth);
  f1->SetParameters(Nppl,Np,Nn,Npi,Nnpl,sppl,sp,sn,snpl,totdepth);
  f1->SetParameter(10, 0);
  f1->SetNpx(po);
  
  // electric field is equal to zero out of depletion regions
  f1depl = new TF1("f1depl", "max(f1, 0)", 0, totdepth);
  f1depl->SetNpx(po);
  
  // ionization coefficients
  fa = new TF1("fa", this, &SiStructure::alpha_vs_depth, 0, totdepth, 0, "SiStructure", "alpha_vs_depth");
  fa->SetNpx(po);
  
  fb = new TF1("fb", this, &SiStructure::beta_vs_depth, 0, totdepth, 0, "SiStructure", "beta_vs_depth");
  fb->SetNpx(po);
  
  fab = new TF1("fab", this, &SiStructure::alphabeta_vs_depth, 0, totdepth, 0, "SiStructure", "alphabeta_vs_depth");
  fab->SetNpx(po);
  
  print();
  
  return true;
}

void SiStructure::print() const
{
  cout << "Structure:\n"
       << "p+" << setw(12) << Nppl << " " << sppl << "\n"
       << "p " << setw(12) << Np   << " " << sp << "\n"
       << "n " << setw(12) << Nn   << " " << sn << "\n"
       << "pi" << setw(12) << Npi  << " -\n"
       << "n+" << setw(12) << Nnpl << " " << snpl << "\n"
       << "totdepth = " << totdepth << "\n"
       << endl;
}

string SiStructure::str() const
{
  ostringstream oss;
  oss  << "" << Nppl << "@" << sppl << " "
       << "" << Np   << "@" << sp << " "
       << "" << Nn   << "@" << sn << " "
       << "" << Npi  << "@- "
       << "" << Nnpl << "@" << snpl << " "
       << "depth = " << totdepth;
  
  return oss.str();
}

double SiStructure::setBias(const double voltage)
{
  cout << "SiStructure::setBias() voltage = " << voltage << endl;
  
  // search for target bias voltage with secant method:
  // https://en.wikipedia.org/wiki/Secant_method
  
  const double eps = 1e-6;
  const int maxiter = 50;
  
  // two initial values
  double x0 = 0;
  f1->SetParameter(10, x0);
  f1depl->SetParameter(10, x0);
  double y0 = f1depl->Integral(0, totdepth) / 10000 - voltage;
  int i = 0;
  //cout << "s#" << i << ": E=" << x0 << " Bias diff=" << y0 << " V" << endl;
  
  double x1 = (y0 / totdepth) * 1e4;
  f1->SetParameter(10, x1);
  f1depl->SetParameter(10, x1);
  double y1 = f1depl->Integral(0, totdepth) / 10000 - voltage;
  i++;
  //cout << "s#" << i << ": E=" << x1 << " Bias diff=" << y1 << " V" << endl;
  
  // adjust electric field to match target bias voltage
  while (true) {
    const double xN = x1 - y1 * (x1 - x0) / (y1 - y0);
    
    f1->SetParameter(10, xN);
    f1depl->SetParameter(10, xN);
    
    const double V = f1depl->Integral(0, totdepth) / 10000;
    const double yN = V - voltage;
    i++;
    
    //cout << "s#" << i << ": E=" << xN << " Bias diff=" << yN << " V" << endl;
    
    if (fabs(yN) < eps) {
      cout << "Target voltage is reached, set voltage = " << V << " (error = " << yN << ")\n" << endl;
      return V;
    }
    
    x0 = x1; y0 = y1;
    x1 = xN; y1 = yN;
    
    // check to avoid endless loop if method does not converge
    if ((i > maxiter) && (y0*y1 < 0)) break;
    if (i > 2*maxiter) {
      cout << "WARNING: problem to reach target voltage with good precision, set voltage = " << V << " (error = " << yN << ")\n" << endl;
      return V;
    }
  }
  
  // continue to improve precision with bisection method
  // https://en.wikipedia.org/wiki/Bisection_method
  
  // the root value guaranteed to be in the interval [x0, x1]
  
  while (true) {
    const double xN = 0.5 * (x0 + x1);
    
    f1->SetParameter(10, xN);
    f1depl->SetParameter(10, xN);
    
    const double V = f1depl->Integral(0, totdepth) / 10000;
    const double yN = V - voltage;
    i++;
    
    //cout << "b#" << i << ": E=" << xN << " Bias diff=" << yN << " V"
    //     << " interval = [" << x0 << " " << x1 << "] width=" << (x0-x1) << endl;
    
    if (fabs(yN) < eps) {
      cout << "Target voltage is reached, set voltage = " << V << " (error = " << yN << ")\n" << endl;
      return V;
    }
    
    // set new interval
    if (y0*yN > 0) {
      x0 = xN; y0 = yN;
    }
    else {
      x1 = xN; y1 = yN;
    }
    
    // check to avoid endless loop if method does not converge
    if (i > 3*maxiter) {
      cout << "WARNING: problem to reach target voltage with good precision, set voltage = " << V << " (error = " << yN << ")\n" << endl;
      return V;
    }
  }
}

void SiStructure::calcDepletion()
{
  // calculate depletion region
  cout << "SiStructure::calcDepletion()" << endl;
  
  depl.clear();
  
  vector<double> p;
  double x0 = f1->GetXmin();
  double y0 = f1->Eval(x0);
  
  // add starting point
  if (y0 > 0) p.push_back(x0);
  
  // search the roots of f1 function
  for (int j = 1; j <= f1->GetNpx(); ++j) {
    const double x = f1->GetXmin() + (f1->GetXmax() - f1->GetXmin()) * j / f1->GetNpx();
    const double y = f1->Eval(x);
    
    if (y0 * y < 0) p.push_back(f1->GetX(0, x0, x));
    
    x0 = x;
    y0 = y;
    
    //cout << "i=" << i << " x=" << x << " y=" << y << endl;
  }
  
  // add ending point
  if (y0 > 0) p.push_back(x0);
  
  if (p.size() % 2 == 1) {
    cout << "ERROR: failed to calculate depletion zone" << endl;
    return;
  }
  
  // split edge points into ranges
  for (size_t k = 0; k < p.size()/2; ++k) {
    const SiRange r = {p[2*k], p[2*k+1]};
    depl.push_back(r);
  }
  
  cout << "Depletion regions:\n";
  for (size_t i = 0; i < depl.size(); ++i) {
    cout << (i+1) << ": " << depl[i].min << " " << depl[i].max << " size=" << depl[i].size() << "\n";
  }
  cout << "total size = " << totalDepl() << "\n"
       << endl;
}

double SiStructure::totalDepl() const
{
  double sum = 0;
  for (size_t i = 0; i < depl.size(); ++i) sum += depl[i].size();
  return sum;
}

void SiStructure::calcGain()
{
  cout << "SiStructure::calcGain()" << endl;
  
  // compute function gabI(x) = integrate[a(x) - b(x), {x, 0, totdepth}]
  TGraph* gabI = new TGraph(fab, "i");
  
  // compute integral I0L = integrate[a(x) * exp(-gabI(x)), {x, 0, totdepth}]
  const Int_t N = gabI->GetN();
  const Double_t* x = gabI->GetX();
  const Double_t* y = gabI->GetY();
  Double_t I0L = 0;
  
  for (Int_t i = 0; i < N-1; i++) {
    
    // work-around for non-depl. regions
    //if (!(f1depl->Eval(x[i]) > 0)) continue;
    
    const double a0 = fa->Eval(x[i]);
    //if (!TMath::Finite(a0)) a0 = 0;
    
    const double y0 = a0 * exp(-0.0001 * y[i]);
    const double y1 = fa->Eval(x[i+1]) * exp(-0.0001 * y[i+1]);
    const double S0 = 0.5 * (y0 + y1) * (x[1] - x[0]);
    
    I0L += S0;
    
    /*
    // pring debug output only for first and last 10 iterations
    if ((10 < i) && (i < N-10)) continue;
    
    cout << "i=" << i
         << " x0=" << x[i]
         << " x1=" << x[i+1]
         << " f1depl(x0)=" << f1depl->Eval(x[i])
         << " f1depl(x0)>0=" << (f1depl->Eval(x[i]) > 0)
         << " a(x0)=" << fa->Eval(x[i])
         << " a(x1)=" << fa->Eval(x[i+1])
         << " y0=" << y0
         << " y1=" << y1
         << " S0=" << S0
         << " I0L=" << I0L
         << endl;
    */
  }
  
  I0L *= 0.0001;
  
  // compute gain(x) = exp(-gabI(x)) / (1 - I0L)
  Double_t* M = new Double_t[N];
  for (Int_t i = 0; i < N-1; i++) {
    M[i] = exp(-0.0001 * y[i]) / (1 - I0L);
  }
  
  gM = new TGraph(N-1, x, M);
  
  cout << "gain[0] = " << M[0]
       << " gain[1] = " << M[1]
       << " gain[20] = " << M[20]
       << "\n"
       << endl;
}

double SiStructure::alpha_vs_efield(const double e) const
{
  if (e > 0) return 2300*exp(-6.78*(2e5/e - 1));
  return 0;
}

double SiStructure::beta_vs_efield(const double e) const
{
  if (e > 0) return 13*exp(-13.2*(2e5/e - 1));
  return 0;
}

Double_t SiStructure::alpha_vs_depth(Double_t* x, Double_t* /*par*/)
{
  const Double_t e = f1->Eval(x[0]);
  return alpha_vs_efield(e);
}

Double_t SiStructure::beta_vs_depth(Double_t* x, Double_t* /*par*/)
{
  const Double_t e = f1->Eval(x[0]);
  return beta_vs_efield(e);
}

Double_t SiStructure::alphabeta_vs_depth(Double_t* x, Double_t* par)
{
  return alpha_vs_depth(x, par) - beta_vs_depth(x, par);
}
