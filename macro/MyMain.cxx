#include "MyMain.h"

#include <TApplication.h>
#include <TSystem.h>
#include <TGScrollBar.h>
#include <TROOT.h>

#include <iostream>
#include <fstream>
using namespace std;

MyMainFrame::MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h)
: TGMainFrame(p, w, h)
{
  // Create a main frame with a number of different buttons.

  gROOT->ProcessLine(".L ./DopFieldG.C"); 
  gROOT->ProcessLine(".L ./GainDeplG.C"); 
  gROOT->ProcessLine(".L ./avalanche.C");
  gROOT->ProcessLine(".L ./avalancherev.C");
  //  gROOT->ProcessLine(".L ./elavalanche.C");
  gROOT->ProcessLine(".L ./avalancheMG.C");
  gROOT->ProcessLine(".L ./avalancheMrevG.C");
  gROOT->ProcessLine(".L ./avalancheMvarG.C");
  
  //  TGCanvas *fCanvasWindow = new TGCanvas(this, 900, 380);
  
  //============Structure=======================
  TGGroupFrame* stru = new TGGroupFrame(this, "Structure", kVerticalFrame);
  const UInt_t entrywidth=60;
  TString eline;
  ifstream fin("structure.txt");
  eline.ReadLine(fin); 
  eNppl=new TGTextEntry(stru, eline.Data(), 11);
  eNppl->Resize(entrywidth,0);
  eline.ReadLine(fin);
  eNp=new TGTextEntry(stru, eline.Data(), 12);
  eNp->Resize(entrywidth,0);
  eline.ReadLine(fin);
  eNn=new TGTextEntry(stru, eline.Data(), 13);
  eNn->Resize(entrywidth,0);
  eline.ReadLine(fin);
  eNpi=new TGTextEntry(stru, eline.Data(), 14);
  eNpi->Resize(entrywidth,0);
  eline.ReadLine(fin);
  eNnpl=new TGTextEntry(stru, eline.Data(), 15);
  eNnpl->Resize(entrywidth,0);
  eline.ReadLine(fin);
  esppl=new TGTextEntry(stru, eline.Data(), 16);
  esppl->Resize(entrywidth,0);
  eline.ReadLine(fin);
  esp=new TGTextEntry(stru, eline.Data(), 17);
  esp->Resize(entrywidth,0);
  eline.ReadLine(fin);
  esn=new TGTextEntry(stru, eline.Data(), 18);
  esn->Resize(entrywidth,0);
  eline.ReadLine(fin);
  esnpl=new TGTextEntry(stru, eline.Data(), 19);
  esnpl->Resize(entrywidth,0);
  eline.ReadLine(fin);
  etotdepth=new TGTextEntry(stru, eline.Data(), 20);
  etotdepth->Resize(entrywidth,0);
  fin.close();
  
  TGGroupFrame* inpt = new TGGroupFrame(this, "Input parameters", kVerticalFrame);
  fst_point=new TGTextEntry(inpt,"1",70);
  fst_point->Resize(entrywidth,0);
  light=new TGTextEntry(inpt,"1",71);
  light->Resize(entrywidth,0);
  sta=new TGTextEntry(inpt,"1000",72);
  sta->Resize(entrywidth,0);
  max_g=new TGTextEntry(inpt,"5000",73);
  max_g->Resize(entrywidth,0);
  E_thr=new TGTextEntry(inpt,"150000",74);
  E_thr->Resize(entrywidth,0);
  outh=new TGTextEntry(inpt,"out1.txt",75);
  outh->Resize(entrywidth,0);
  outp=new TGTextEntry(inpt,"out2.txt",76);
  outp->Resize(entrywidth,0);
  //============================================

  //================Knopki=================
  TGGroupFrame *act=new TGGroupFrame(this, "Action", kVerticalFrame);
  act->SetLayoutManager(new TGMatrixLayout(act, 0, 2, 6));
  // act->Resize(150,204);
  // cout<<act->GetWidth()<<" "<<act->GetHeight()<<endl;
  
  fButton1 = new TGTextButton(act, " &DopField.      V=", 1);
  fButton1->Associate(this);
  fButton1->Resize(108,fButton1->GetHeight());
  act->AddFrame(fButton1);
  fEntry = new TGTextEntry(act, "300.0", 7);
  fEntry->Resize(entrywidth-20,0);
  act->AddFrame(fEntry);

  fButton4 = new TGTextButton(act, "Gain,  depl width", 50);
  fButton4->Associate(this);
  fButton4->Resize(108,fButton4->GetHeight());
  //fButton4->SetWidth(108);
  act->AddFrame(fButton4);
  fEntry4 = new TGTextEntry(act, "", 81); 
  fEntry4->Resize(entrywidth-20,0);
  act->AddFrame(fEntry4);

  fButton5 = new TGTextButton(act, "Aval,  E(kV/cm)=", 82);
  fButton5->Associate(this);
  fButton5->Resize(108,fButton5->GetHeight());
  act->AddFrame(fButton5);
  fEntry5 = new TGTextEntry(act, "250", 83); 
  fEntry5->Resize(entrywidth-20,0);
  act->AddFrame(fEntry5);

  fButton6 = new TGTextButton(act, " Avalanche multi", 84);
  fButton6->Associate(this);
  fButton6->Resize(108,fButton6->GetHeight());
  act->AddFrame(fButton6);
  fEntry6 = new TGTextEntry(act, "", 85); 
  fEntry6->Resize(entrywidth-20,0);
  act->AddFrame(fEntry6);

  fButton7 = new TGTextButton(act, "Aval mult (E-var)", 86);
  fButton7->Associate(this);
  fButton7->Resize(108,fButton7->GetHeight());
  act->AddFrame(fButton7);
  fEntry7 = new TGTextEntry(act, "", 87); 
  fEntry7->Resize(entrywidth-20,0);
  act->AddFrame(fEntry7);
  
  fButton8 = new TGTextButton(act, "&Save structure", 88); 
  fButton8->Associate(this);
  fButton8->Resize(90,fButton8->GetHeight());
  act->AddFrame(fButton8);
  //act->Resize();
  //cout<<act->GetWidth()<<" "<<act->GetHeight()<<endl;; 
  
  //================Width and ab length of particle=======
  TGGroupFrame *wl=new TGGroupFrame(this, "Widths and absorb length, um", kVerticalFrame);
  TString wline;
  ifstream fwl("wl.txt");
  wline.ReadLine(fwl);
  fstw=new TGTextEntry(wl, wline.Data(), 101);
  fstw->Resize(entrywidth-20,0);
  wline.ReadLine(fwl);
  scdw=new TGTextEntry(wl, wline.Data(), 102);
  scdw->Resize(entrywidth-20,0);
  wline.ReadLine(fwl);
  thdw=new TGTextEntry(wl, wline.Data(), 103);
  thdw->Resize(entrywidth-20,0);
  wline.ReadLine(fwl);
  fthw=new TGTextEntry(wl, wline.Data(), 104);
  fthw->Resize(entrywidth-20,0);
  wline.ReadLine(fwl);
  abl=new TGTextEntry(wl, wline.Data(), 105);
  abl->Resize(entrywidth-20,0);
  fwl.close();
  wl->SetLayoutManager(new TGMatrixLayout(wl, 0, 5, 2));
  wl->AddFrame(new TGLabel(wl, "   1st"));
  wl->AddFrame(new TGLabel(wl, "  2nd "));
  wl->AddFrame(new TGLabel(wl, "  field"));
  wl->AddFrame(new TGLabel(wl, "   4th"));
  wl->AddFrame(new TGLabel(wl, "ab. len"));
  wl->AddFrame(fstw);
  wl->AddFrame(scdw);
  wl->AddFrame(thdw);
  wl->AddFrame(fthw);
  wl->AddFrame(abl);

  //===========Voltages===============
  TGGroupFrame *vol=new TGGroupFrame(this, "Voltages"); //, kVerticalFrame);
  entryVStart = new TGTextEntry(vol, "150", 21);
  entryVStart->Resize(entrywidth-20,0);
  vol->AddFrame(entryVStart);
  
  entryVEnd = new TGTextEntry(vol, "350", 22);
  entryVEnd->Resize(entrywidth-20,0);
  vol->AddFrame(entryVEnd);
 
  entryVStep = new TGTextEntry(vol, "10", 23);
  entryVStep->Resize(entrywidth-20,0);
  vol->AddFrame(entryVStep);
 
  //=======================================
  // TGLayoutHints *fLayoutact = new TGLayoutHints(kLHintsCenterX | kLHintsTop);
  // this->AddFrame(vol); //,fLayoutact);

  
  fPicBut = new TGPictureButton(this, gClient->GetPicture("./apd.xpm"), 3);
  fPicBut->Resize(50,42);
  
  stru->SetLayoutManager(new TGMatrixLayout(stru, 0, 2, 6));
  stru->AddFrame(new TGLabel(stru, "N0 p+"));
  stru->AddFrame(eNppl);
  stru->AddFrame(new TGLabel(stru, "N0 p"));
  stru->AddFrame(eNp);
  stru->AddFrame(new TGLabel(stru, "N0 n"));
  stru->AddFrame(eNn);
  stru->AddFrame(new TGLabel(stru, "N0 pi"));
  stru->AddFrame(eNpi);
  stru->AddFrame(new TGLabel(stru, "N0 n+"));
  stru->AddFrame(eNnpl);
  stru->AddFrame(new TGLabel(stru, "sigma p+"));
  stru->AddFrame(esppl);
  stru->AddFrame(new TGLabel(stru, "sigma p"));
  stru->AddFrame(esp);
  stru->AddFrame(new TGLabel(stru, "sigma n"));
  stru->AddFrame(esn);
  stru->AddFrame(new TGLabel(stru, "sigma n+"));
  stru->AddFrame(esnpl);
  stru->AddFrame(new TGLabel(stru, "tot width, um"));
  stru->AddFrame(etotdepth);

  inpt->SetLayoutManager(new TGMatrixLayout(inpt, 0, 2, 6));
  inpt->AddFrame(new TGLabel(inpt, "1st point"));
  inpt->AddFrame(fst_point);
  inpt->AddFrame(new TGLabel(inpt, "ab length, mkm"));
  inpt->AddFrame(light);
  inpt->AddFrame(new TGLabel(inpt, "stat"));
  inpt->AddFrame(sta);
  inpt->AddFrame(new TGLabel(inpt, "max gain"));
  inpt->AddFrame(max_g);
  inpt->AddFrame(new TGLabel(inpt, "E_threshold"));
  inpt->AddFrame(E_thr);
  inpt->AddFrame(new TGLabel(inpt, "histo file"));
  inpt->AddFrame(outh);
  inpt->AddFrame(new TGLabel(inpt, "param file"));
  inpt->AddFrame(outp);

  TGLayoutHints *fLayout = new TGLayoutHints(kLHintsTop);
  //AddFrame(fPicBut, fLayout);
  AddFrame(stru, fLayout);
  AddFrame(inpt, fLayout);
  AddFrame(wl, fLayout);
  AddFrame(act, fLayout);
  AddFrame(vol, fLayout);
  
  SetLayoutManager(new TGHorizontalLayout(this));
  SetWindowName("APD simulating");
  SetIconName("APD simulating");
  MapSubwindows();
  MapWindow();
  Layout();
  //Resize();    
  //MapSubwindows();
}

void MyMainFrame::save_voltage(const char* fname) const
{
  vector<double> list;
  
  const double start = atof(entryVStart->GetText());
  const double end = atof(entryVEnd->GetText());
  const double step = atof(entryVStep->GetText());
  
  for (double v = start; v < end; v += step)
    list.push_back(v);
  
  ofstream fvolt(fname);
  fvolt << list.size() << endl;
  for (size_t i = 0; i < list.size(); ++i)
    fvolt << list[i] << endl;
}

Bool_t MyMainFrame::ProcessMessage(Long_t msg, Long_t parm1, Long_t)
{
  // Process events generated by the buttons in the frame.
  
  switch (GET_MSG(msg)) {
  case kC_COMMAND:
    switch (GET_SUBMSG(msg)) {
    case kCM_BUTTON:
      if (parm1 == 1) {
       ofstream fstr("structure.txt");
      fstr<<eNppl->GetText()<<endl;
      fstr<<eNp->GetText()<<endl;
      fstr<<eNn->GetText()<<endl;
      fstr<<eNpi->GetText()<<endl;
      fstr<<eNnpl->GetText()<<endl;
      fstr<<esppl->GetText()<<endl;
      fstr<<esp->GetText()<<endl;
      fstr<<esn->GetText()<<endl;
      fstr<<esnpl->GetText()<<endl;
      fstr<<etotdepth->GetText()<<endl;
      fstr.close();

      TString sdop1="DopField(",sdop3=")";
      //      Connect(fButton1, "Pressed()", 0, 0, (sdop1+fEntry->GetText()+sdop3).Data());  
      gROOT->ProcessLine((sdop1+fEntry->GetText()+sdop3).Data());
      // cout<<fEntry->GetText()<<endl;
      }
      else if(parm1 == 50) { 
      ofstream fstr("structure.txt");
      fstr<<eNppl->GetText()<<endl;
      fstr<<eNp->GetText()<<endl;
      fstr<<eNn->GetText()<<endl;
      fstr<<eNpi->GetText()<<endl;
      fstr<<eNnpl->GetText()<<endl;
      fstr<<esppl->GetText()<<endl;
      fstr<<esp->GetText()<<endl;
      fstr<<esn->GetText()<<endl;
      fstr<<esnpl->GetText()<<endl;
      fstr<<etotdepth->GetText()<<endl;
      fstr.close();

      save_voltage("voltages.txt");
      gROOT->ProcessLine("GainDepl()");
      }
      else if(parm1 == 82) {
      Double_t fie=atof(fEntry5->GetText());
      if(fie>=0){
        TString saval="avalanche(\"",sc="\"", s0="000",smg=max_g->GetText(),sf=")",sz=",",sw=etotdepth->GetText();
        gROOT->ProcessLine((saval+outh->GetText()+sc+sz+fEntry5->GetText()+s0+sz+sw+sz+fst_point->GetText()+sz+sta->GetText()+sz+smg+sf).Data());
      }
      else{
        TString saval="avalancherev(\"",sc="\"", s0="000",smg=max_g->GetText(),sf=")",sz=",",sw=etotdepth->GetText();
        gROOT->ProcessLine((saval+outh->GetText()+sc+sz+&(fEntry5->GetText())[1]+s0+sz+sw+sz+fst_point->GetText()+sz+sta->GetText()+sz+smg+sf).Data());
      }
      }
      else if(parm1 == 84) { 
      save_voltage("voltages.txt");
      ofstream fwlo("wl.txt");
      fwlo<<fstw->GetText()<<endl;
      fwlo<<scdw->GetText()<<endl;
      fwlo<<thdw->GetText()<<endl;
      fwlo<<fthw->GetText()<<endl;
      fwlo<<abl->GetText()<<endl;
      fwlo.close();
      Double_t fie=atof(entryVStart->GetText());
      if(fie>=0){
        TString savaM="avalancheMG(\"",sc="\"",smg=max_g->GetText(),sf=")",sz=",",sw=etotdepth->GetText();
        gROOT->ProcessLine((savaM+outh->GetText()+sc+sz+sc+outp->GetText()+sc+sz+sw+sz+light->GetText()+sz+sta->GetText()+sz+smg+sf).Data());
      }
      else{
        TString savaM="avalancheMrevG(\"",sc="\"",smg=max_g->GetText(),sf=")",sz=",",sw=etotdepth->GetText();
        gROOT->ProcessLine((savaM+outh->GetText()+sc+sz+sc+outp->GetText()+sc+sz+sw+sz+light->GetText()+sz+sta->GetText()+sz+smg+sf).Data());
      }
      }
      else if(parm1 == 86) {
      ofstream fstr("structure.txt");
      fstr<<eNppl->GetText()<<endl;
      fstr<<eNp->GetText()<<endl;
      fstr<<eNn->GetText()<<endl;
      fstr<<eNpi->GetText()<<endl;
      fstr<<eNnpl->GetText()<<endl;
      fstr<<esppl->GetText()<<endl;
      fstr<<esp->GetText()<<endl;
      fstr<<esn->GetText()<<endl;
      fstr<<esnpl->GetText()<<endl;
      fstr<<etotdepth->GetText()<<endl;
      fstr.close();

      save_voltage("voltages.txt");
 
      TString savMv="avalancheMvarG(\"",sc="\"",smg=max_g->GetText(),sf=")",sz=",",sthr=E_thr->GetText();
      gROOT->ProcessLine((savMv+outh->GetText()+sc+sz+sc+outp->GetText()+sc+sz+light->GetText()+sz+sthr+sz+sta->GetText()+sz+smg+sf).Data());
      }
      else if(parm1 == 88) { 
      ofstream fstrG("structure.txt");
      fstrG<<eNppl->GetText()<<endl;
      fstrG<<eNp->GetText()<<endl;
      fstrG<<eNn->GetText()<<endl;
      fstrG<<eNpi->GetText()<<endl;
      fstrG<<eNnpl->GetText()<<endl;
      fstrG<<esppl->GetText()<<endl;
      fstrG<<esp->GetText()<<endl;
      fstrG<<esn->GetText()<<endl;
      fstrG<<esnpl->GetText()<<endl;
      fstrG<<etotdepth->GetText()<<endl;
      fstrG.close();
      ofstream fwlo("wl.txt");
      fwlo<<fstw->GetText()<<endl;
      fwlo<<scdw->GetText()<<endl;
      fwlo<<thdw->GetText()<<endl;
      fwlo<<fthw->GetText()<<endl;
      fwlo<<abl->GetText()<<endl;
      fwlo.close();
      }
      break;
    case kCM_CHECKBUTTON:
      printf("check button id %ld pressed\n", parm1);
      break;
    case kCM_RADIOBUTTON:
      if (parm1 == 5)
      fRBut2->SetState(kButtonUp);
      if (parm1 == 6)
      fRBut1->SetState(kButtonUp);
      printf("radio button id %ld pressed\n", parm1);
      break;
    default:
      break;
    }
  default:
    break;
  }
  return kTRUE;
}

void MyMainFrame::CloseWindow()
{
  //DeleteWindow();
  gApplication->Terminate(0);
}
