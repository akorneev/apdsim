#include <TGClient.h>
#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGLabel.h>
#include <TString.h>
#include <iostream>
#include <TGCanvas.h>

class MyMainFrame : public TGMainFrame {

private:
  TGTextButton      *fButton1,*fButton4,*fButton5,*fButton8,*fButton6,*fButton7,*fButton1_x;
  TGPictureButton   *fPicBut;
  TGCheckButton     *fChkBut;
  TGRadioButton     *fRBut1, *fRBut2;
  TGTextEntry       *fEntry, *fEntry4, *fEntry5,*fEntry8, *fEntry6, *fEntry7;
  TGTextEntry *entryVStart, *entryVEnd, *entryVStep;
  TGTextEntry *eNppl, *eNp,*eNn,*eNpi,*eNnpl,*esppl,*esp,*esn,*esnpl,*etotdepth, *fst_point, *light, *sta, *max_g, *E_thr, *outh, *outp;
  TGTextEntry       *fstw, *scdw, *thdw, *fthw, *abl;
  
  void save_voltage(const char* fname) const;

public:
    MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h);
    //~MyMainFrame();// need to delete here created widgets
    Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);
    void CloseWindow();
};
