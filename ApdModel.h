#include <TMatrix.h>
#include <TH2F.h>

class Apd
{
public:
  // P - cross talk full probability
  // R - ration of diagonal to cross pixels probability
  Apd(const Int_t nx, const Int_t ny, const Float_t P, const Float_t R)
  : fNx(nx), fNy(ny), fCnt(0), fMap(nx, ny)
  {
    fProbC = 0.25 * P / (1 + R);
    fProbD = R * fProbC;
  }
  
  // sample event for given number of photons
  Int_t SampleEvent (Int_t nPhot);
  
  // fill hist map
  void FillMap (TH2F* h2) const
  {
    for (Int_t x = 0; x < fNx; ++x)
      for (Int_t y = 0; y < fNy; ++y)
        if (fMap(x, y)) h2->Fill(x, y);
  }
  
  Float_t getFullCrosstalkProbability() const {return 4 * (fProbC + fProbD);}

protected:
  void SampleOnePhoton(Int_t x, Int_t y);
  
  // sample cross talk from given pixel
  void CrossCurrent (Int_t x, Int_t y);
  
  Int_t    fNx;      //number of pixels along x
  Int_t    fNy;      //number of pixels along y
  Float_t  fProbC;   // probability of cross talk with cross pixels
  Float_t  fProbD;   // probability of cross talk with diagonal pixels
  
  Int_t    fCnt;     //fired pixels counter
  TMatrixF fMap;     //fired pixels map
};
